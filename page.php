<?php include 'header.php' ?>

    <section class="breadcrumb">
        <div class="container">
            <h1>Page Title</h1>
            <ul>
                <li class="trail-begin"><a href="#">Home</a></li>
                <li class="trail-end">Page Title</li>
            </ul>
        </div>
    </section>


    <div id="content" class="blog-post">
        <div class="container">
            <div class="row">
                <div id="primary" class="content-area col-xs-12 col-sm-12 col-lg-12">
                    <main id="main" class="site-main">
                        <article>
                            <div class="post-image">
                                <img src="assets/images/blog-4.jpg" alt="article-img"/>
                            </div>
                            <div class="box">

                                <div class="post-title">
                                    <h3><a href="#">Most strong remedy for diabetes</a></h3>
                                </div>

                                <div class="post-the-content ">
                                    <p>I neglect my talents Far far away, behind the word mountains, far from the
                                        countries Vokalia and Consonantia, there live the blind texts. Separated they
                                        live in Bookmarksgrove right at the coast of the Semantics, a large language
                                        ocean. A small river named Duden flows by their place and supplies it with the
                                        necessary regelialia ...</p>
                                </div>

                            </div>
                        </article>

                    </main>
                </div>


            </div>
        </div>
    </div>

<?php include 'footer.php' ?>