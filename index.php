<?php
include 'header.php'
?>

<section class="slider">
    <div id="main-slider" class="owl-carousel owl-theme">
        <div class="item"><img src="assets/images/slider-5.png">
            <div class="slider-caption" data-animation-in="fadeInUp" data-animation-out="animate-out fadeOutDown">
                <h2>Fitness</h2>
                <span>is new sexy</span>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore
                    et</p>
                <a>Join Now</a>
            </div>
        </div>
        <div class="item"><img src="assets/images/slider-1.png">
            <div class="slider-caption" data-animation-in="fadeInUp" data-animation-out="animate-out fadeOutDown">
                <h2>Fitness</h2>
                <span>is new sexy</span>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore
                    et</p>
                <a>Join Now</a>
            </div>
        </div>
        <div class="item"><img src="assets/images/slider-4.png">
            <div class="slider-caption" data-animation-in="fadeInUp" data-animation-out="animate-out fadeOutDown">
                <h2>Fitness</h2>
                <span>is new sexy</span>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore
                    et</p>
                <a>Join Now</a>
            </div>
        </div>
        <div class="item"><img src="assets/images/slider-6.png">
            <div class="slider-caption" data-animation-in="fadeInUp" data-animation-out="animate-out fadeOutDown">
                <h2>Fitness</h2>
                <span>is new sexy</span>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore
                    et</p>
                <a>Join Now</a>
            </div>
        </div>
    </div>
</section>

<section class="introduction">
    <div class="container">
        <div class="row">
            <div class="box">
                <figure>
                    <img src="assets/images/about.png" alt="about"/>
                </figure>
                <div class="description">

                    <h2>ABOUT GYMFIT</h2>
                    <span>No pain No gain</span>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestib ulum porttitor egestas orci,
                        vitae
                        ullamcorper risus consectetur id. Donec at velit vestibulum, rutrum massa quis, porttitor lorem.
                        Donec et
                        ultricies arcu. In odio augue, hendrerit nec nisl ac, rhoncus gravida mauris.</p>
                    <button>Join Now</button>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="offer">
    <div class="offer-parallax-window" data-parallax="scroll" data-image-src="assets/images/parallex.jpg">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <h2>Bring all your friends</h2>
                    <span>get exciting discount</span>

                    <button>Join Today</button>
                </div>
            </div>
        </div>
    </div>

</div>

<section class="courses">
    <div class="container">
        <div class="row">
            <h2>Our courses</h2>
            <span>eos et accusamus et iusto odio </span>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="fp-block-wrapper">
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 courseList">
                    <a href="#">

                        <div class="box">
                            <figure>
                                <img src="assets/images/icon-1.png" alt="courses">
                            </figure>
                            <h3>ABOUT GYMFIT</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestib ulum porttitor </p>
                        </div>
                    </a>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 courseList">
                    <a href="#">

                        <div class="box">
                            <figure>
                                <img src="assets/images/icon-2.png" alt="courses">
                            </figure>
                            <h3>ABOUT GYMFIT</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestib ulum porttitor </p>
                        </div>
                    </a>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 courseList">
                    <a href="#">

                        <div class="box">
                            <figure>
                                <img src="assets/images/icon-3.png" alt="courses">
                            </figure>
                            <h3>ABOUT GYMFIT</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestib ulum porttitor </p>
                        </div>
                    </a>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 courseList">
                    <a href="#">

                        <div class="box">
                            <figure>
                                <img src="assets/images/icon-4.png" alt="courses">
                            </figure>
                            <h3>ABOUT GYMFIT</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestib ulum porttitor </p>
                        </div>
                    </a>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 courseList">
                    <a href="#">

                        <div class="box">
                            <figure>
                                <img src="assets/images/icon-5.png" alt="courses">
                            </figure>
                            <h3>ABOUT GYMFIT</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestib ulum porttitor </p>
                        </div>
                    </a>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 courseList">
                    <a href="#">
                        <div class="box">
                            <figure>
                                <img src="assets/images/icon-6.png" alt="courses">
                            </figure>
                            <h3>ABOUT GYMFIT</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestib ulum porttitor </p>
                        </div>
                    </a>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 courseList">
                    <a href="#">
                        <div class="box">
                            <figure>
                                <img src="assets/images/icon-7.png" alt="courses">
                            </figure>
                            <h3>ABOUT GYMFIT</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestib ulum porttitor </p>
                        </div>
                    </a>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 courseList">
                    <a href="#">
                        <div class="box">
                            <figure>
                                <img src="assets/images/icon-8.png" alt="courses">
                            </figure>
                            <h3>ABOUT GYMFIT</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestib ulum porttitor </p>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="video">
    <div id="youtube-video">
        <div class="overlay-video"></div>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2>ABOUT GYMFIT</h2>
                    <span>No pain No gain</span>
                </div>
            </div>
            <div class="row registration-form">
                <div class="col-lg-6 col-md-6 col-sm-12 ">
                    <input type="text" placeholder="Full Name">
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 ">
                    <input type="text" placeholder="Email Address">
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 ">
                    <input type="text" placeholder="Phone Number">
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 ">
                    <input type="text" placeholder="Date">
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 ">
                    <textarea rows="5" placeholder="Your message ..."></textarea>

                    <div class="read-more text-center">
                        <button type="submit">Join Today</button>
                    </div>
                </div>


            </div>
        </div>
    </div>
</section>

<section class="timetable">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 timetable-title">
                <h2>Time Table</h2>
                <span>No pain No gain</span>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 routine_table">
                <table class="table ">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">sunday</th>
                        <th scope="col">monday</th>
                        <th scope="col">tuesday</th>
                        <th scope="col">wednesday</th>
                        <th scope="col">thursday</th>
                        <th scope="col">friday</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <th scope="row">6:00 <br> AM</th>
                        <td>
                            <figure>
                                <img src="assets/images/icon-1.png" alt="courses">
                            </figure>

                            <div class="">
                                <h5>Legs</h5>
                                <span>Mesuck</span>
                            </div>
                        </td>
                        <td>
                            <figure>
                                <img src="assets/images/icon-2.png" alt="courses">
                            </figure>
                            <h5>Abs</h5><span>Rebicca</span></td>
                        <td>
                            <figure>
                                <img src="assets/images/icon-3.png" alt="courses">
                            </figure>
                            <h5>Chest</h5> <span>Christi</span></td>
                        <td>
                            <figure>
                                <img src="assets/images/icon-4.png" alt="courses">
                            </figure>
                            <h5>Power Lift</h5> <span>Malbika</span></td>
                        <td>
                            <figure>
                                <img src="assets/images/icon-5.png" alt="courses">
                            </figure>
                            <h5>Sqat</h5> <span>Rachel</span></td>
                        <td>
                            <figure>
                                <img src="assets/images/icon-6.png" alt="courses">
                            </figure>
                            <h5>Weight Loss</h5> <span>Mark</span></td>
                    </tr>
                    <tr>
                        <th scope="row">6:00 <br> AM</th>
                        <td>
                            <figure>
                                <img src="assets/images/icon-9.png" alt="courses">
                            </figure>

                            <div class="">
                                <h5>Legs</h5>
                                <span>Mesuck</span>
                            </div>
                        </td>
                        <td>
                            <figure>
                                <img src="assets/images/icon-10.png" alt="courses">
                            </figure>
                            <h5>Power Lift</h5> <span>Malbika</span></td>
                        <td>
                            <figure>
                                <img src="assets/images/icon-11.png" alt="courses">
                            </figure>
                            <h5>Sqat</h5> <span>Rachel</span></td>
                        <td>
                            <figure>
                                <img src="assets/images/icon-12.png" alt="courses">
                            </figure>
                            <h5>Chest</h5> <span>Christi</span></td>
                        <td>
                            <figure>
                                <img src="assets/images/icon-13.png" alt="courses">
                            </figure>
                            <h5>Power Lift</h5> <span>Malbika</span></td>
                        <td>
                            <figure>
                                <img src="assets/images/icon-14.png" alt="courses">
                            </figure>
                            <h5>Sqat</h5> <span>Rachel</span></td>

                    </tr>
                    <tr>
                        <th scope="row">8:00 <br> AM</th>
                        <td>
                            <figure>
                                <img src="assets/images/icon-1.png" alt="courses">
                            </figure>

                            <div class="">
                                <h5>Legs</h5>
                                <span>Mesuck</span>
                            </div>
                        </td>
                        <td>
                            <figure>
                                <img src="assets/images/icon-2.png" alt="courses">
                            </figure>
                            <h5>Abs</h5><span>Rebicca</span></td>
                        <td>
                            <figure>
                                <img src="assets/images/icon-3.png" alt="courses">
                            </figure>
                            <h5>Chest</h5> <span>Christi</span></td>
                        <td>
                            <figure>
                                <img src="assets/images/icon-4.png" alt="courses">
                            </figure>
                            <h5>Power Lift</h5> <span>Malbika</span></td>
                        <td>
                            <figure>
                                <img src="assets/images/icon-5.png" alt="courses">
                            </figure>
                            <h5>Squat</h5> <span>Rachel</span></td>
                        <td>
                            <figure>
                                <img src="assets/images/icon-6.png" alt="courses">
                            </figure>
                            <h5>Weight Loss</h5> <span>Mark</span></td>
                    </tr>
                    <tr>
                        <th scope="row">9:00 <br> AM</th>
                        <td>
                            <figure>
                                <img src="assets/images/icon-9.png" alt="courses">
                            </figure>

                            <div class="">
                                <h5>Legs</h5>
                                <span>Mesuck</span>
                            </div>
                        </td>
                        <td>
                            <figure>
                                <img src="assets/images/icon-10.png" alt="courses">
                            </figure>
                            <h5>Power Lift</h5> <span>Malbika</span></td>
                        <td>
                            <figure>
                                <img src="assets/images/icon-11.png" alt="courses">
                            </figure>
                            <h5>Sqat</h5> <span>Rachel</span></td>
                        <td>
                            <figure>
                                <img src="assets/images/icon-12.png" alt="courses">
                            </figure>
                            <h5>Chest</h5> <span>Christi</span></td>
                        <td>
                            <figure>
                                <img src="assets/images/icon-13.png" alt="courses">
                            </figure>
                            <h5>Power Lift</h5> <span>Malbika</span></td>
                        <td>
                            <figure>
                                <img src="assets/images/icon-14.png" alt="courses">
                            </figure>
                            <h5>Squat</h5> <span>Rachel</span></td>
                    </tr>
                    </tbody>
                </table>
            </div>


        </div>
    </div>
</section>

<section class="front-gallery">
    <div class="gallery-title">
        <div class="gallery-text-wrapper">
            <h2>Our Gallery <span>EOS ET ACCUSAMUS ET IUSTO ODIO</span></h2>

        </div>
    </div>
    <div class="box"><a href="assets/images/gallery-1.png">
            <div class="overlay-gallery">+</div>
            <img src="assets/images/gallery-1.png"> </a></div>
    <div class="box"><a href="assets/images/gallery-2.png">
            <div class="overlay-gallery">+</div>
            <img src="assets/images/gallery-2.png"> </a></div>
    <div class="box"><a href="assets/images/gallery-3.png">
            <div class="overlay-gallery">+</div>
            <img src="assets/images/gallery-3.png"> </a></div>
    <div class="box"><a href="assets/images/gallery-4.png">
            <div class="overlay-gallery">+</div>
            <img src="assets/images/gallery-4.png"> </a></div>
    <div class="box"><a href="assets/images/gallery-5.png">
            <div class="overlay-gallery">+</div>
            <img src="assets/images/gallery-5.png"> </a></div>
    <div class="box"><a href="assets/images/gallery-6.png">
            <div class="overlay-gallery">+</div>
            <img src="assets/images/gallery-6.png"> </a></div>
    <div class="box"><a href="assets/images/gallery-7.png">
            <div class="overlay-gallery">+</div>
            <img src="assets/images/gallery-7.png"> </a></div>
    <div class="box"><a href="assets/images/gallery-8.png">
            <div class="overlay-gallery">+</div>
            <img src="assets/images/gallery-8.png"> </a></div>
    <div class="box"><a href="assets/images/gallery-9.png">
            <div class="overlay-gallery">+</div>
            <img src="assets/images/gallery-9.png"> </a></div>
    <div class="box"><a href="assets/images/gallery-10.png">
            <div class="overlay-gallery">+</div>
            <img src="assets/images/gallery-10.png"> </a></div>
</section>

<section class="trainers">
    <div class="container">
        <div class="row">
            <div class="container">
                <h2>Our Trainers</h2>
                <span>EOS ET ACCUSAMUS ET IUSTO ODIO</span>
            </div>
        </div>

        <div id="trainers-slider" class="owl-carousel owl-theme">
            <div class="item">
                <div class="box">
                    <figure>
                        <img src="assets/images/trainer-1.png" alt=""/>
                    </figure>
                    <div class="description">
                        <h3>Salena Wales <br><span>Abs specilist</span></h3>
                        <p> iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti
                            quos
                            dolores </p>

                        <p> iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti
                            quos
                            dolores </p>
                        <ul class="social-icon">
                            <li class="social-facebook"><a href="#"><i class="fab fab fa-facebook"></i></a></li>
                            <li class="social-twitter"><a href="#"><i class="fab fab fa-twitter"></i></a></li>
                            <li class="social-instagram"><a href="#"><i class="fab fab fa-instagram"></i></a></li>
                            <li class="social-youtube"><a href="#"><i class="fab fab fa-youtube"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="box">
                    <figure>
                        <img src="assets/images/trainer-2.png" alt=""/>
                    </figure>
                    <div class="description">
                        <h3>Salena Wales <br><span>Abs specilist</span></h3>
                        <p> iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti
                            quos
                            dolores </p>

                        <p> iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti
                            quos
                            dolores </p>
                        <ul class="social-icon">
                            <li class="social-facebook"><a href="#"><i class="fab fab fa-facebook"></i></a></li>
                            <li class="social-twitter"><a href="#"><i class="fab fab fa-twitter"></i></a></li>
                            <li class="social-instagram"><a href="#"><i class="fab fab fa-instagram"></i></a></li>
                            <li class="social-youtube"><a href="#"><i class="fab fab fa-youtube"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>


<?php include 'footer.php' ?>
