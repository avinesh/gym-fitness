<?php include 'header.php' ?>
<section class="breadcrumb">
    <div class="container">
        <h1>Single Page</h1>
        <ul>
            <li class="trail-begin"><a href="#">Home</a></li>
            <li class="trail-end">Single Page</li>
        </ul>
    </div>
</section>
<section class="introduction">
    <div class="container">
        <div class="row">
            <div class="box">
                <div class="description">
                    <figure>
                        <img src="assets/images/about.png" alt="about"/>
                    </figure>
                    <h2>ABOUT GYMFIT</h2>
                    <span>No pain No gain</span>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestib ulum porttitor egestas orci,
                        vitae
                        ullamcorper risus consectetur id. Donec at velit vestibulum, rutrum massa quis, porttitor lorem.
                        Donec et
                        ultricies arcu. In odio augue, hendrerit nec nisl ac, rhoncus gravida mauris.</p>
                    <a href="">Join Now</a>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section-call-to-action background-img" style="background: url('assets/images/aboutus-bg.png')">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h3><a href="#"> Looking for a excellent fitness center ?</a></h3>
                <p>Nullam vulputate lorem ut leo. Sed volutpat. Etiam non pede. Nullam et mauris. Praesent sed elit.
                    Nulla posuere. Etiam sit amet turpis. Nullam mattis libero non</p>
                <a href="#" class="readmore">Know more</a>
            </div>
        </div>
    </div>
</section>


<section class="events">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h2>Events</h2>
                <span>No pain No gain</span>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4 col-sm-4 col-xs-4 fp-event-list-wrapper">
                <div class="fp-event-list  box-shadow-block">
                    <figure>
                        <a href="#"><img src="assets/images/listing-3.jpg" alt=""></a>
                    </figure>
                    <div class="fp-event-description">
                        <h4><a href="#">Private Group Lessons</a></h4>
                        <p>Nullam vulputate lorem ut leo. Sed volutpat. Etiam non pede. Nullam et mauris.</p>
                        <a href="#" class="more-link">Know More</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-sm-4 col-xs-4 fp-event-list-wrapper">
                <div class="fp-event-list  box-shadow-block">
                    <figure>
                        <a href="#"><img src="assets/images/listing-3.jpg" alt=""></a>
                    </figure>
                    <div class="fp-event-description">
                        <h4><a href="#">Private Group Lessons</a></h4>
                        <p>Nullam vulputate lorem ut leo. Sed volutpat. Etiam non pede. Nullam et mauris.</p>
                        <a href="#" class="more-link">Know More</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-sm-4 col-xs-4 fp-event-list-wrapper">
                <div class="fp-event-list  box-shadow-block">
                    <figure>
                        <a href="#"><img src="assets/images/listing-3.jpg" alt=""></a>
                    </figure>
                    <div class="fp-event-description">
                        <h4><a href="#">Private Group Lessons</a></h4>
                        <p>Nullam vulputate lorem ut leo. Sed volutpat. Etiam non pede. Nullam et mauris.</p>
                        <a href="#" class="more-link">Know More</a>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>

<?php include 'footer.php' ?>
