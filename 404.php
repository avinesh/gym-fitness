<?php include 'header.php' ?>
<section class="error-page section">
    <div class="container">
        <div class="row">
            <div id="primary" class="content-area">
                <main id="main" class="site-main">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-6 col-md-offset-3 col-sm-12 col-xs-12">
                                <div class="error-inner">
                                    <h1>404<span>Oops! That page can&rsquo;t be found.</h1>

                                    <p>It looks like nothing was found at this location. Maybe try one of the links
                                        below or a search?</p>
                                    <a class="btn primary" href=""><i class="fa fa-long-arrow-left"></i>Go to home</a>
                                </div>
                            </div>
                        </div>
                    </div>

                </main><!-- #main -->
            </div><!-- #primary -->
        </div>
    </div>
</section>

<?php include 'footer.php' ?>
