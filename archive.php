<?php include 'header.php' ?>

    <section class="breadcrumb">
        <div class="container">
            <h1>Our Archive</h1>
            <ul>
                <li class="trail-begin"><a href="#">Home</a></li>
                <li class="trail-end">Archive</li>
            </ul>
        </div>
    </section>


    <section class="post-listing">
        <div class="container">
            <div class="col-lg-9 col-sm-9 col-xs-12">
                <article>
                    <div class="post-image">
                        <img src="assets/images/listing-1.jpg" alt="article-img"/>
                    </div>
                    <div class="box">

                        <div class="post-title">
                            <h3><a href="#">Most strong remedy for diabetes</a></h3>
                        </div>
                        <div class="postmeta">
                            <ul>
                                <li><span class="author vcard"><a href=""> Avinesh Shakya</a></span></li>
                                <li><span class="author vcard"><a href=""> July 8 , 2018</a></span></li>
                                <li><span class="author vcard"><a href=""> Food / fitness</a></span></li>
                            </ul>
                        </div>

                        <div class="post-the-content ">
                            <p>I neglect my talents Far far away, behind the word mountains, far from the
                                countries Vokalia and Consonantia, there live the blind texts. Separated they
                                live in Bookmarksgrove right at the coast of the Semantics...</p>
                            <button>Continue Reading</button>
                        </div>

                    </div>
                </article>
                <article>
                    <div class="post-image">
                        <img src="assets/images/listing-2.jpg" alt="article-img"/>
                    </div>
                    <div class="box">

                        <div class="post-title">
                            <h3><a href="#">Most strong remedy for diabetes</a></h3>
                        </div>
                        <div class="postmeta">
                            <ul>
                                <li><span class="author vcard"><a href=""> Avinesh Shakya</a></span></li>
                                <li><span class="author vcard"><a href=""> July 8 , 2018</a></span></li>
                                <li><span class="author vcard"><a href=""> Food / fitness</a></span></li>
                            </ul>
                        </div>

                        <div class="post-the-content ">
                            <p>I neglect my talents Far far away, behind the word mountains, far from the
                                countries Vokalia and Consonantia, there live the blind texts. Separated they
                                live in Bookmarksgrove right at the coast of the Semantics...</p>
                            <button>Continue Reading</button>
                        </div>

                    </div>
                </article>
                <article>
                    <div class="post-image">
                        <img src="assets/images/listing-3.jpg" alt="article-img"/>
                    </div>
                    <div class="box">

                        <div class="post-title">
                            <h3><a href="#">Most strong remedy for diabetes</a></h3>
                        </div>
                        <div class="postmeta">
                            <ul>
                                <li><span class="author vcard"><a href=""> Avinesh Shakya</a></span></li>
                                <li><span class="author vcard"><a href=""> July 8 , 2018</a></span></li>
                                <li><span class="author vcard"><a href=""> Food / fitness</a></span></li>
                            </ul>
                        </div>

                        <div class="post-the-content ">
                            <p>I neglect my talents Far far away, behind the word mountains, far from the
                                countries Vokalia and Consonantia, there live the blind texts. Separated they
                                live in Bookmarksgrove right at the coast of the Semantics...</p>
                            <button>Continue Reading</button>
                        </div>

                    </div>
                </article>

            </div>

            <aside id="secondary" class="sidebar widget-area col-xs-12 col-sm-3 ">
                <div class="widget popular-post">
                    <h3>Popular post</h3>
                    <ul>
                        <li>
                            <a href="#">
                                <figure>
                                    <img src="assets/images/gallery-1.png " alt="">
                                </figure>
                                <h4>Dominion under fourth</h4>
                                <span>Sep, 26 2015.</span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <figure>
                                    <img src="assets/images/gallery-2.png " alt="">
                                </figure>
                                <h4>Dominion under fourth</h4>
                                <span>Sep, 26 2015.</span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <figure>
                                    <img src="assets/images/gallery-3.png " alt="">
                                </figure>
                                <h4>Dominion under fourth</h4>
                                <span>Sep, 26 2015.</span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <figure>
                                    <img src="assets/images/gallery-4.png" alt="">
                                </figure>
                                <h4>Dominion under fourth</h4>
                                <span>Sep, 26 2015.</span>
                            </a>
                        </li>

                    </ul>
                </div>

                <div class="widget category-sidebar">
                    <h3>Catagories</h3>
                    <ul>
                        <li>
                            <a href="#">
                                <h4>Charity (8)</h4>

                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <h4>Images (12)</h4>

                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <h4>Music Top (10)</h4>

                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <h4>Travel (11)</h4>

                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <h4>web Design (5)</h4>

                            </a>
                        </li>
                    </ul>
                </div>


            </aside>

        </div>

    </section>

<?php include 'footer.php' ?>