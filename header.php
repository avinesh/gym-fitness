<!DOCTYPE html>
<html>
<head>
    <title>Page Title</title>

    <link href="assets/library/font-awesome/css/fontawsome.css" rel="stylesheet">

    <link href="https://fonts.googleapis.com/css?family=Raleway:300,300i,400,400i,500,500i,600,600i,700,700i,800"
          rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lato:300,300i,400,400i,700,700i" rel="stylesheet">
    <link href="assets/library/font-awesome/css/fontawsome.css" rel="stylesheet" type="text/css">

    <link href="assets/library/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="assets/library/owlcarousel/css/owl.carousel.min.css" rel="stylesheet" type="text/css">
    <link href="assets/library/owlcarousel/css/owl.theme.default.min.css" rel="stylesheet" type="text/css">

    <link href="assets/css/animate.css" rel="stylesheet" type="text/css">
    <link href="assets/library/simple-lightbox/css/simplelightbox.min.css" rel="stylesheet" type="text/css">
    <link href="style.css" rel="stylesheet" type="text/css">


</head>
<body>


<nav class="navbar navbar-default main-navigation">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header ">
            <button type="button" class="menu-nav-toggle">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <div class="header-logo site-branding">
                <h1 class="site-title">
                    <a href="#" rel="home">
                        Fitness Gym</a>
                </h1>
                <p class="site-description">just another wordpress theme</p>
            </div>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" >

            <ul id="primary-menu" class="menu nav-menu nav navbar-nav navbar-right" aria-expanded="false" style="height: auto">
                <li><a href="index.php">Home</a>
               
                </li>
                <li>
                    <a href="aboutus.php">About </a>
                </li>
                <li>
                    <a href="#">Service</a>
                </li>

                <li class="menu-item-has-children ">
                    <a href="#">Page</a>
                    <ul class="sub-menu">
                        <li><a href="faq.php">Faq page</a></li>
                        <li><a href="404.php">404 page</a></li>
                        <li><a href="single.php">Single page</a>
                            <ul class="sub-menu">
                                <li><a href="single.php">Full width</a></li>
                                <li><a href="single-2.php">With sidebar</a></li>

                            </ul>
                        </li>


                    </ul>
                </li>
                <li class="menu-item-has-children ">
                    <a href="#">Contact</a>
                    <ul class="sub-menu">
                        <li><a href="contact.php">Contact Layout 1</a></li>
                        <li><a href="contact-2.php">Contact Layout 2</a></li>

                    </ul>
                </li>
            </ul>


        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>