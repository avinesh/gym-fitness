<?php include 'header2.php' ?>

<section class="slider">
    <div id="main-slider" class="owl-carousel main-slider owl-theme">
        <div class="item"><img src="assets/images/slider-5.png">
            <div class="slider-caption" data-animation-in="fadeInUp" data-animation-out="animate-out fadeOutDown">
                <h2>Fitness</h2>
                <span>is new sexy</span>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore
                    et</p>
                <a>Join Now</a>
            </div>
        </div>
        <div class="item"><img src="assets/images/slider-1.png">
            <div class="slider-caption" data-animation-in="fadeInUp" data-animation-out="animate-out fadeOutDown">
                <h2>Fitness</h2>
                <span>is new sexy</span>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore
                    et</p>
                <a>Join Now</a>
            </div>
        </div>
        <div class="item"><img src="assets/images/slider-4.png">
            <div class="slider-caption" data-animation-in="fadeInUp" data-animation-out="animate-out fadeOutDown">
                <h2>Fitness</h2>
                <span>is new sexy</span>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore
                    et</p>
                <a>Join Now</a>
            </div>
        </div>
        <div class="item"><img src="assets/images/slider-6.png">
            <div class="slider-caption" data-animation-in="fadeInUp" data-animation-out="animate-out fadeOutDown">
                <h2>Fitness</h2>
                <span>is new sexy</span>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore
                    et</p>
                <a>Join Now</a>
            </div>
        </div>
    </div>
</section>

<section class="introduction">
    <div class="container">
        <div class="row">
            <div class="box">
                <div class="description">
                    <figure>
                        <img src="assets/images/about.png" alt="about"/>
                    </figure>
                    <h2>ABOUT GYMFIT</h2>
                    <span>No pain No gain</span>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestib ulum porttitor egestas orci,
                        vitae
                        ullamcorper risus consectetur id. Donec at velit vestibulum, rutrum massa quis, porttitor lorem.
                        Donec et
                        ultricies arcu. In odio augue, hendrerit nec nisl ac, rhoncus gravida mauris.</p>
                    <button>Join Now</button>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="offer">
    <div class="offer-parallax-window" data-parallax="scroll" data-image-src="assets/images/parallex.jpg">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <h2>Bring all your friends</h2>
                    <span>get exciting discount</span>

                    <button>Join Today</button>
                </div>
            </div>
        </div>
    </div>

</div>

<section class="courses">
    <div class="container">
        <div class="row">
            <h2>Our courses</h2>
            <span>eos et accusamus et iusto odio </span>
        </div>
    </div>

    <div class="fp-block-wrapper">
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 courseList">
            <a href="#">
                <div class="overlay-course">
                    <img src="assets/images/course-1.png" alt="courses">
                </div>
                <div class="box">
                    <figure>
                        <img src="assets/images/icon-1.png" alt="courses">
                    </figure>
                    <h3>ABOUT GYMFIT</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestib ulum porttitor </p>
                </div>
            </a>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 courseList">
            <a href="#">
                <div class="overlay-course">
                    <img src="assets/images/course-2.png" alt="courses">
                </div>
                <div class="box">
                    <figure>
                        <img src="assets/images/icon-2.png" alt="courses">
                    </figure>
                    <h3>ABOUT GYMFIT</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestib ulum porttitor </p>
                </div>
            </a>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 courseList">
            <a href="#">
                <div class="overlay-course">
                    <img src="assets/images/course-3.png" alt="courses">
                </div>
                <div class="box">
                    <figure>
                        <img src="assets/images/icon-3.png" alt="courses">
                    </figure>
                    <h3>ABOUT GYMFIT</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestib ulum porttitor </p>
                </div>
            </a>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 courseList">
            <a href="#">
                <div class="overlay-course">
                    <img src="assets/images/course-4.png" alt="courses">
                </div>
                <div class="box">
                    <figure>
                        <img src="assets/images/icon-4.png" alt="courses">
                    </figure>
                    <h3>ABOUT GYMFIT</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestib ulum porttitor </p>
                </div>
            </a>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 courseList">
            <a href="#">
                <div class="overlay-course">
                    <img src="assets/images/course-5.png" alt="courses">
                </div>
                <div class="box">
                    <figure>
                        <img src="assets/images/icon-5.png" alt="courses">
                    </figure>
                    <h3>ABOUT GYMFIT</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestib ulum porttitor </p>
                </div>
            </a>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 courseList">
            <a href="#">
                <div class="overlay-course">
                    <img src="assets/images/course-6.png" alt="courses">
                </div>
                <div class="box">
                    <figure>
                        <img src="assets/images/icon-6.png" alt="courses">
                    </figure>
                    <h3>ABOUT GYMFIT</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestib ulum porttitor </p>
                </div>
            </a>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 courseList">
            <a href="#">
                <div class="overlay-course">
                    <img src="assets/images/course-7.png" alt="courses">
                </div>
                <div class="box">
                    <figure>
                        <img src="assets/images/icon-7.png" alt="courses">
                    </figure>
                    <h3>ABOUT GYMFIT</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestib ulum porttitor </p>
                </div>
            </a>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 courseList">
            <a href="#">
                <div class="overlay-course">
                    <img src="assets/images/course-8.png" alt="courses">
                </div>
                <div class="box">
                    <figure>
                        <img src="assets/images/icon-8.png" alt="courses">
                    </figure>
                    <h3>ABOUT GYMFIT</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestib ulum porttitor </p>
                </div>
            </a>
        </div>
    </div>
</section>

<section class="video">
    <div id="youtube-video">
        <div class="overlay-video"></div>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2>ABOUT GYMFIT</h2>
                    <span>No pain No gain</span>
                </div>
            </div>
            <div class="row registration-form">
                <div class="col-lg-6 col-md-6 col-sm-12 ">
                    <input type="text" placeholder="Full Name">
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 ">
                    <input type="text" placeholder="Email Address">
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 ">
                    <input type="text" placeholder="Phone Number">
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 ">
                    <input type="text" placeholder="Date">
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 ">
                    <textarea rows="5" placeholder="Your message ..."></textarea>

                    <div class="read-more text-center">
                        <button type="submit">Join Today</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="fitness-park-classes">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 timetable-title">
                <h2>Our Classes</h2>
                <span>No pain No gain</span>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4 col-sm-4 col-xs-4 fp-classes-list">
                <figure>
                    <a href="#"><img src="assets/images/listing-1.jpg" alt=""/></a>
                </figure>
                <div class="fp-classes-description">
                    <h4><a href="#">Private & Group Lessons</a></h4>
                    <p>19/05/15 - 01:00pm - 03:00pm</p>
                </div>
            </div>
            <div class="col-lg-4 col-sm-4 col-xs-4 fp-classes-list">
                <figure>
                    <a href="#"><img src="assets/images/listing-2.jpg" alt=""/></a>
                </figure>
                <div class="fp-classes-description">
                    <h4><a href="#">Private & Group Lessons</a></h4>
                    <p>19/05/15 - 01:00pm - 03:00pm</p>
                </div>
            </div>
            <div class="col-lg-4 col-sm-4 col-xs-4 fp-classes-list">
                <figure>
                    <a href="#"><img src="assets/images/listing-3.jpg" alt=""/></a>
                </figure>
                <div class="fp-classes-description">
                    <h4><a href="#">Private & Group Lessons</a></h4>
                    <p>19/05/15 - 01:00pm - 03:00pm</p>
                </div>
            </div>
            <div class="col-lg-4 col-sm-4 col-xs-4 fp-classes-list">
                <figure>
                    <a href="#"><img src="assets/images/gallery-2.png" alt=""/></a>
                </figure>
                <div class="fp-classes-description">
                    <h4><a href="#">Private & Group Lessons</a></h4>
                    <p>19/05/15 - 01:00pm - 03:00pm</p>
                </div>
            </div>
            <div class="col-lg-4 col-sm-4 col-xs-4 fp-classes-list">
                <figure>
                    <a href="#"><img src="assets/images/gallery-4.png" alt=""/></a>
                </figure>
                <div class="fp-classes-description">
                    <h4><a href="#">Private & Group Lessons</a></h4>
                    <p>19/05/15 - 01:00pm - 03:00pm</p>
                </div>
            </div>
            <div class="col-lg-4 col-sm-4 col-xs-4 fp-classes-list">
                <figure>
                    <a href="#"><img src="assets/images/gallery-1.png" alt=""/></a>
                </figure>
                <div class="fp-classes-description">
                    <h4><a href="#">Private & Group Lessons</a></h4>
                    <p>19/05/15 - 01:00pm - 03:00pm</p>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="fitness-park-pricing">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h2>Our Gallery</h2>
                <span>No pain No gain</span>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4 col-sm-4 col-xs-4">
                <div class="box">
                    <div class="fp-pricing-title">Standard</div>
                    <div class="fp-pricing-monthly">$50<span>/month</span></div>
                    <div class="fp-pricing-features">
                        <ul>
                            <li>Free Hand</li>
                            <li>Gym Fitness</li>
                            <li>Running</li>
                            <li>---------</li>
                            <li>---------</li>
                        </ul>
                    </div>
                    <div class="fp-pricing-button">
                        <button>Join Now</button>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-sm-4 col-xs-4">
                <div class="box">
                    <div class="fp-pricing-title">Premium</div>
                    <div class="fp-pricing-monthly">$70<span>/month</span></div>
                    <div class="fp-pricing-features">
                        <ul>
                            <li>Free Hand</li>
                            <li>Gym Fitness</li>
                            <li>Running</li>
                            <li>Yoga</li>
                            <li>---------</li>
                        </ul>
                    </div>
                    <div class="fp-pricing-button">
                        <button>Join Now</button>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-sm-4 col-xs-4">
                <div class="box">
                    <div class="fp-pricing-title">PLATINUM</div>
                    <div class="fp-pricing-monthly">$70<span>/month</span></div>
                    <div class="fp-pricing-features">
                        <ul>
                            <li>Free Hand</li>
                            <li>Gym Fitness</li>
                            <li>Running</li>
                            <li>Yoga</li>
                            <li>Body Building</li>
                        </ul>
                    </div>
                    <div class="fp-pricing-button">
                        <button>Join Now</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="fitness-park-blog-post-front">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h2>Our Blog</h2>
                <span>No pain No gain</span>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-4 fp-blog-list">
                <figure>
                    <img src="assets/images/gallery-2.png" alt=""/>
                    <span class="cat-links"><a href="http://localhost/fitness-park/category/uncategorized/" rel="category tag">Uncategorized</a></span>
                </figure>
                <div class="fp-blog-description">
                    <h4 class="post-title"><a href="#">Daily Squat</a></h4>
                    <div class="post-content"><p>At vero eos et accusamus et iusto odio placeat facere possimus, omnis
                            voluptas assumenda est</p></div>
                    <span class="posted-on"><a href="#" rel="bookmark"><time class="entry-date published updated" datetime="2019-01-28T04:22:32+00:00">January 28, 2019</time></a></span>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 fp-blog-list">
                <figure>
                    <img src="assets/images/gallery-3.png" alt=""/>
                    <span class="cat-links"><a href="http://localhost/fitness-park/category/uncategorized/" rel="category tag">Uncategorized</a></span>
                </figure>
                <div class="fp-blog-description">
                    <h4 class="post-title"><a href="#">Abs Workout</a></h4>
                    <div class="post-content"><p>At vero eos et accusamus et iusto odio placeat facere possimus, omnis
                            voluptas assumenda est</p></div>
                    <span class="posted-on"><a href="#" rel="bookmark"><time class="entry-date published updated" datetime="2019-01-28T04:22:32+00:00">January 28, 2019</time></a></span>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 fp-blog-list">
                <figure>
                    <img src="assets/images/gallery-4.png" alt=""/>
                    <span class="cat-links"><a href="http://localhost/fitness-park/category/uncategorized/" rel="category tag">Uncategorized</a></span>
                </figure>
                <div class="fp-blog-description">
                    <h4 class="post-title"><a href="#">Leg Day</a></h4>
                    <div class="post-content"><p>At vero eos et accusamus et iusto odio placeat facere possimus, omnis
                            voluptas assumenda est</p></div>
                    <span class="posted-on"><a href="#" rel="bookmark"><time class="entry-date published updated" datetime="2019-01-28T04:22:32+00:00">January 28, 2019</time></a></span>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 fp-blog-list">
                <figure>
                    <img src="assets/images/gallery-2.png" alt=""/>
                    <span class="cat-links"><a href="http://localhost/fitness-park/category/uncategorized/" rel="category tag">Uncategorized</a></span>
                </figure>
                <div class="fp-blog-description">
                    <h4 class="post-title"><a href="#">Daily Squat</a></h4>
                    <div class="post-content"><p>At vero eos et accusamus et iusto odio placeat facere possimus, omnis
                            voluptas assumenda est</p></div>
                    <span class="posted-on"><a href="#" rel="bookmark"><time class="entry-date published updated" datetime="2019-01-28T04:22:32+00:00">January 28, 2019</time></a></span>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 fp-blog-list">
                <figure>
                    <img src="assets/images/gallery-3.png" alt=""/>
                    <span class="cat-links"><a href="http://localhost/fitness-park/category/uncategorized/" rel="category tag">Uncategorized</a></span>
                </figure>
                <div class="fp-blog-description">
                    <h4 class="post-title"><a href="#">Abs Workout</a></h4>
                    <div class="post-content"><p>At vero eos et accusamus et iusto odio placeat facere possimus, omnis
                            voluptas assumenda est</p></div>
                    <span class="posted-on"><a href="#" rel="bookmark"><time class="entry-date published updated" datetime="2019-01-28T04:22:32+00:00">January 28, 2019</time></a></span>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 fp-blog-list">
                <figure>
                    <img src="assets/images/gallery-4.png" alt=""/>
                    <span class="cat-links"><a href="http://localhost/fitness-park/category/uncategorized/" rel="category tag">Uncategorized</a></span>
                </figure>
                <div class="fp-blog-description">
                    <h4 class="post-title"><a href="#">Leg Day</a></h4>
                    <div class="post-content"><p>At vero eos et accusamus et iusto odio placeat facere possimus, omnis
                            voluptas assumenda est</p></div>
                    <span class="posted-on"><a href="#" rel="bookmark"><time class="entry-date published updated" datetime="2019-01-28T04:22:32+00:00">January 28, 2019</time></a></span>
                </div>
            </div>
        </div>
    </div>

</section>

<section class="fitness-park-gallery front-gallery">

        <div class="container">
            <div class="row">
                <div class=" col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <h2>Our Gallery</h2>
                    <span>No pain No gain</span>
                </div>
            </div>
            <div class="row">
                <div align="center">
                    <button class="btn btn-default filter-button" data-filter="all">All</button>
                    <button class="btn btn-default filter-button" data-filter="hdpe">HDPE Pipes</button>
                    <button class="btn btn-default filter-button" data-filter="sprinkle">Sprinkle Pipes</button>
                    <button class="btn btn-default filter-button" data-filter="spray">Spray Nozzle</button>
                    <button class="btn btn-default filter-button" data-filter="irrigation">Irrigation Pipes</button>
                </div>
                <br/>

                <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter hdpe">
                    <div class="box-2">
                        <a href="assets/images/gallery-6.png">
                            <div class="overlay-gallery">+</div>
                            <img src="assets/images/gallery-6.png" alt=""/></a>
                    </div>
                </div>

                <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter sprinkle">
                    <div class="box-2">
                        <a href="assets/images/gallery-6.png">
                            <div class="overlay-gallery">+</div>
                            <img src="assets/images/gallery-6.png" alt=""/></a>
                    </div>
                </div>

                <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter hdpe">
                    <div class="box-2">
                        <a href="assets/images/gallery-6.png">
                            <div class="overlay-gallery">+</div>
                            <img src="assets/images/gallery-6.png" alt=""/></a>
                    </div>
                </div>

                <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter irrigation">
                    <div class="box-2">
                        <a href="assets/images/gallery-6.png">
                            <div class="overlay-gallery">+</div>
                            <img src="assets/images/gallery-6.png" alt=""/></a>
                    </div>
                </div>

                <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter spray">
                    <div class="box-2">
                        <a href="assets/images/gallery-6.png">
                            <div class="overlay-gallery">+</div>
                            <img src="assets/images/gallery-6.png" alt=""/></a>
                    </div>
                </div>

                <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter irrigation">
                    <a href="assets/images/gallery-6.png">
                        <div class="overlay-gallery">+</div>
                        <img src="assets/images/gallery-6.png" alt=""/></a>
                </div>

                <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter spray">
                    <div class="box-2">
                        <a href="assets/images/gallery-6.png">
                            <div class="overlay-gallery">+</div>
                            <img src="assets/images/gallery-6.png" alt=""/></a>
                    </div>
                </div>

                <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter irrigation">
                    <div class="box-2">
                        <a href="assets/images/gallery-6.png">
                            <div class="overlay-gallery">+</div>
                            <img src="assets/images/gallery-6.png" alt=""/></a>
                    </div>
                </div>

                <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter irrigation">
                    <div class="box-2">
                        <a href="assets/images/gallery-6.png">
                            <div class="overlay-gallery">+</div>
                            <img src="assets/images/gallery-6.png" alt=""/></a>
                    </div>
                </div>

                <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter hdpe">
                    <div class="box-2">
                        <a href="assets/images/gallery-6.png">
                            <div class="overlay-gallery">+</div>
                            <img src="assets/images/gallery-6.png" alt=""/></a>
                    </div>
                </div>

                <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter spray">
                    <div class="box-2">
                        <a href="assets/images/gallery-6.png">
                            <div class="overlay-gallery">+</div>
                            <img src="assets/images/gallery-6.png" alt=""/></a>
                    </div>
                </div>

                <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter sprinkle">
                    <div class="box-2">
                        <a href="assets/images/gallery-6.png">
                            <div class="overlay-gallery">+</div>
                            <img src="assets/images/gallery-6.png" alt=""/></a>
                    </div>
                </div>
            </div>
        </div>

</section>

<section class="fitness-park-trainers">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h2>Our Trainers</h2>
                <span>EOS ET ACCUSAMUS ET IUSTO ODIO</span>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-4">
                <div class="fp_team_box">
                        <figure>
                            <img src="assets/images/listing-1.jpg" alt=""/>
                        </figure>
                        <div class="fp_description">
                            <h4><a href="#">Jesicca James<a></h4>
                            <p>Cardio Trainer</p>
                            <ul class="social-icon-plain">
                                <li class="social-facebook"><a href="#"><i class="fab fab fa-facebook"></i></a></li>
                                <li class="social-twitter"><a href="#"><i class="fab fab fa-twitter"></i></a></li>
                                <li class="social-instagram"><a href="#"><i class="fab fab fa-instagram"></i></a></li>
                                <li class="social-youtube"><a href="#"><i class="fab fab fa-youtube"></i></a></li>
                            </ul>
                        </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4">
                <div class="fp_team_box">
                        <figure>
                            <img src="assets/images/listing-3.jpg" alt=""/>
                        </figure>
                        <div class="fp_description">
                            <h4><a href="#">Jesicca James</a></h4>
                            <p>Cardio Trainer</p>
                            <ul class="social-icon-plain">
                                <li class="social-facebook"><a href="#"><i class="fab fab fa-facebook"></i></a></li>
                                <li class="social-twitter"><a href="#"><i class="fab fab fa-twitter"></i></a></li>
                                <li class="social-instagram"><a href="#"><i class="fab fab fa-instagram"></i></a></li>
                                <li class="social-youtube"><a href="#"><i class="fab fab fa-youtube"></i></a></li>
                            </ul>
                        </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4">
                <div class="fp_team_box">
                        <figure>
                            <img src="assets/images/listing-2.jpg" alt=""/>
                        </figure>
                        <div class="fp_description">
                            <h4>Jesicca James</h4>
                            <p>Cardio Trainer</p>
                            <ul class="social-icon-plain">
                                <li class="social-facebook"><a href="#"><i class="fab fab fa-facebook"></i></a></li>
                                <li class="social-twitter"><a href="#"><i class="fab fab fa-twitter"></i></a></li>
                                <li class="social-instagram"><a href="#"><i class="fab fab fa-instagram"></i></a></li>
                                <li class="social-youtube"><a href="#"><i class="fab fab fa-youtube"></i></a></li>
                            </ul>
                        </div>
                </div>
            </div>


        </div>
    </div>
</section>


<?php include 'footer2.php' ?>
