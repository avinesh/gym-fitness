<footer>
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-sm-12 footer_logo">
                <a href="#"><h2>Fitness Gym</h2></a>
                <span>Lift like a man, look like a goddess</span>
            </div>
            <div class="col-lg-12 col-sm-12 col-sm-12 footer_logo">
                <div class="footer-menu">
                    <ul class="footer-menu">
                        <li><a href="#">Home</a></li>
                        <li><a href="#">About</a></li>
                        <li><a href="#">Services</a></li>
                        <li><a href="#">Blog</a></li>
                        <li><a href="#">Pages</a></li>
                        <li><a href="#">Contact</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row bottom_footer">
            <div class="col-lg-4 col-sm-4 col-sm-12">
                <ul class="social-icon">
                    <li class="social-facebook"><a href="#"><i class="fab fab fa-facebook"></i></a></li>
                    <li class="social-twitter"><a href="#"><i class="fab fab fa-twitter"></i></a></li>
                    <li class="social-instagram"><a href="#"><i class="fab fab fa-instagram"></i></a></li>
                    <li class="social-youtube"><a href="#"><i class="fab fab fa-youtube"></i></a></li>

                </ul>
            </div>
            <div class="col-lg-4 col-sm-4 col-sm-12 copyright">
                <p><a href="#">Sparkle Theme.</a> All right reserved 2019</p>
            </div>
            <div class="col-lg-4 col-sm-4 col-sm-12 join_now">
                <button href="#">Join now</button>
            </div>
        </div>
    </div>
</footer>


<script src="assets/js/jquery.min.js"></script>
<script src="assets/library/bootstrap/js/bootstrap.min.js"></script>
<!--<script src="assets/js/jquery.sticky.js"></script>-->
<script src="assets/js/parallex.min.js"></script>
<script src="assets/library/owlcarousel/js/owl.carousel.min.js"></script>
<script src="assets/library/simple-lightbox/js/simple-lightbox.min.js"></script>
<script src="assets/js/jquery.youtubebackground.js"></script>


<script>

    $(function () {
        var selectedClass = "";
        $(".fil-cat").click(function () {
            selectedClass = $(this).attr("data-rel");
            $(".portfolio").fadeTo(100, 0.1);
            $(".portfolio div").not("." + selectedClass).fadeOut().removeClass('scale-anm');
            setTimeout(function () {
                $("." + selectedClass).fadeIn().addClass('scale-anm');
                $(".portfolio").fadeTo(300, 1);
            }, 300);

        });
    });

    /* ********
           sticky nav => ONLY ON FREER
           * ***************/
    /*$(".navbar").sticky({topSpacing:0});*/

    // add animate.css class(es) to the elements to be animated
    function setAnimation(_elem, _InOut) {
        // Store all animationend event name in a string.
        // cf animate.css documentation
        var animationEndEvent = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';

        _elem.each(function () {
            var $elem = $(this);
            var $animationType = 'animated ' + $elem.data('animation-' + _InOut);

            $elem.addClass($animationType).one(animationEndEvent, function () {
                $elem.removeClass($animationType); // remove animate.css Class at the end of the animations
            });
        });
    }

    $(document).ready(function () {
        $('.front-gallery a').simpleLightbox();
        $(".dropdown-toggle").dropdown();

        /**
         * Youtube video BG
         */
        $('#youtube-video').YTPlayer({
            fitToBackground: true,
            videoId: 'WC6W_SAUUK0',
            playerVars: {
                modestbranding: 0,
                autoplay: 1,
                controls: 1,
                showinfo: 0,
                branding: 0,
                rel: 0,
                repeat: true,
                autohide: 0,
                start: 1
            }
        });


        var owl = $('#main-slider');
        owl.owlCarousel({
            loop: true,
            margin: 10,
            autoplay: false,
            autoplayTimeout: 5000,
            nav: true,
            responsive: {
                0: {
                    items: 1
                },
                600: {
                    items: 1
                },
                1000: {
                    items: 1
                }
            }

        });

        var trainers = $('#trainers-slider');
        trainers.owlCarousel({
            loop: true,
            margin: 10,
            autoplay: false,

            nav: true,
            responsive: {
                0: {
                    items: 1
                },
                600: {
                    items: 1
                },
                1000: {
                    items: 1
                }
            }

        });


// Fired before current slide change
        owl.on('change.owl.carousel', function (event) {
            var $currentItem = $('.owl-item', owl).eq(event.item.index);
            var $elemsToanim = $currentItem.find("[data-animation-out]");
            setAnimation($elemsToanim, 'out');
        });

// Fired after current slide has been changed
        var round = 0;
        owl.on('changed.owl.carousel', function (event) {

            var $currentItem = $('.owl-item', owl).eq(event.item.index);
            var $elemsToanim = $currentItem.find("[data-animation-in]");

            setAnimation($elemsToanim, 'in');
        })

        // No animation in other captions so we need to add animation to owl-item while slide changed.

        /* **
         * Sub Menu
         **/
        $('.navbar .menu-item-has-children').append('<span class="sub-toggle"> <i class="fa fa-plus"></i> </span>');
        $('.navbar .page_item_has_children').append('<span class="sub-toggle-children"> <i class="fa fa-plus"></i> </span>');

        $('.navbar .sub-toggle').click(function () {
            $(this).parent('.menu-item-has-children').children('ul.sub-menu').first().slideToggle('1000');
            $(this).children('.fa-plus').first().toggleClass('fa-minus');
        });

        $('.navbar .sub-toggle-children').click(function () {
            $(this).parent('.page_item_has_children').children('ul.sub-menu').first().slideToggle('1000');
            $(this).children('.fa-plus').first().toggleClass('fa-minus');
        });


        $('.menu-nav-toggle').click(function () {
            $(this).toggleClass('open');
            $(this).parent('.navbar-header').next().first().slideToggle('1000');
            ;
        })
        /* ********
        sticky nav => ONLY ON PRO
        ****************/
        /*  var stickyOffset = $('.header-transparent').offset().top;

          $(window).scroll(function () {
              // alert(stickyOffset);
              var sticky = $('.header-transparent'),
                  scroll = $(window).scrollTop();

              if (scroll > 350) sticky.addClass('fixed');
              else sticky.removeClass('fixed');
          });
  */

    });


</script>
</body>
</html>